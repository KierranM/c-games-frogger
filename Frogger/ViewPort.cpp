#include "stdafx.h"
#include "ViewPort.h"


ViewPort::ViewPort(
		Size startSizeInTiles,
		Point startWorldLocation,
		TileMap^ startMap,
		Graphics^ startCanvas
		)
{
	
	sizeInTiles = startSizeInTiles;
	
	worldLocation = startWorldLocation;
	map = startMap;
	int tileSize = map->GetTileSize();
	sizeInPixels = Size(sizeInTiles.Width * tileSize, sizeInTiles.Height * tileSize);
	canvas = startCanvas;
}

void ViewPort::Draw()
{
	//Get the position in tiles
	int tileX = worldLocation.X / map->GetTileSize();
	int tileY = worldLocation.Y / map->GetTileSize();

	//Get the tiles offset
	int xOffset = worldLocation.X % map->GetTileSize();
	int yOffset = worldLocation.Y % map->GetTileSize();

	//Maximum X and Y
	int xLimit = tileX + sizeInTiles.Width;
	int yLimit = tileY + sizeInTiles.Height;

	//Draw the tiles to the screen
	for (int r = tileY - 1; r < yLimit + 1; r++)
	{
		for (int c = tileX - 1; c < xLimit + 1; c++)
		{
			if (r >= 0 && r < map->GetRows())
			{
				if (c >= 0 && c < map->GetColumns())
				{
					Bitmap^ tile = map->GetMapCellBitmap(c,r);
					int xPos = ((c - tileX) * map->GetTileSize()) - xOffset;
					int yPos = ((r - tileY) * map->GetTileSize()) - yOffset;
					canvas->DrawImage(tile, xPos,yPos, map->GetTileSize(), map->GetTileSize());
				}
			}
		}
	}
}

void ViewPort::Move(int x, int y)
{
	if((worldLocation.X + x) >= 0 && (worldLocation.X + x) <= map->GetWidth())
	{
		worldLocation.X += x;
	}
	if((worldLocation.Y+ y) >= 0 && (worldLocation.Y + y) <= map->GetHeight())
	{
		worldLocation.Y += y;
	}
}

void ViewPort::CenterOn(Sprite^ focused)
{
	int halfHeight = (sizeInTiles.Height * map->GetTileSize())/TWO;
	int halfWidth = (sizeInTiles.Width * map->GetTileSize())/TWO;

	Point spriteCenter = focused->GetCenter();
	Point newWorldLocation = Point(spriteCenter.X - halfWidth, spriteCenter.Y - halfHeight);

	if(CheckBoundsX(newWorldLocation.X))
	{
		worldLocation.X = newWorldLocation.X;
	}
	if (CheckBoundsY(newWorldLocation.Y))
	{
		worldLocation.Y = newWorldLocation.Y;
	}
}

bool ViewPort::CheckBoundsX(int xToCheck)
{
	//Checks if the given x value will make the viewport move out of bounds on the X axis

	//Get the position in tiles
	int tileX = xToCheck / map->GetTileSize();

	//Get the tiles offset
	int xOffset = xToCheck % map->GetTileSize();

	//Maximum X
	int xLimit = tileX + sizeInTiles.Width;

	//Added the && statement to fix going one extra column off the left
	if(tileX >= 0 && xToCheck >= 0)
	{
		if (xLimit < map->GetColumns())
		{
			//All conditions are true
			return true;
		}
	}

	//One of the above statements is false so return false
	return false;
}

bool ViewPort::CheckBoundsY(int yToCheck)
{
	//Checks if the given yValue will make the viewport move out of bounds on the Y Axis
	//Get the position in tiles
	int tileY = yToCheck / map->GetTileSize();

	//Get the tiles offset
	int yOffset = yToCheck % map->GetTileSize();

	//Maximum Y
	int yLimit = tileY + sizeInTiles.Height;

	//Added the && statement to fix going one extra row off the bottom
	if (tileY >= 0 && yToCheck >= 0)
	{
		if (yLimit < map->GetRows())
		{
			//All conditions are true
			return true;
		}
	}
		

	//One of the above statements is false so return false
	return false;
}