#include "stdafx.h"
#include "TileMap.h"


TileMap::TileMap(int startRows, int startCols, int startTileSize, TileList^ startList)
{
	rows = startRows;
	columns = startCols;
	tileSize = startTileSize;
	map = gcnew array<int, 2>(startRows, startCols);
	tiles = startList;
}

Bitmap^ TileMap::GetMapCellBitmap(int c, int r)
{
	int tileType = map[r,c];
	Tile^ t = tiles->GetTile(tileType);
	Bitmap^ cell = t->GetTileImage();
	return cell;
}

void TileMap::LoadFromFile(String^ filename)
{
	StreamReader^ file = gcnew StreamReader(filename);
	//Loads the tilemap from the given file
	for (int r = 0; r < rows; r++)
	{
		String^ line = file->ReadLine();
		array<String^>^ parts = line->Split(',');
		for (int c = 0; c < columns; c++)
		{
			map[r,c] = Convert::ToInt32(parts[c]);
		}
	}
}

Tile^ TileMap::GetTile(int xIndex, int yIndex)
{
	//Gets the tile of a given location

	//Get the type of the tile
	int tileType = map[yIndex, xIndex];

	//Get the tile from the list
	Tile^ t = tiles->GetTile(tileType);

	//Return the tile
	return t;
}

bool TileMap::GetIsWalkable(int xIndex, int yIndex)
{
	//Gets the walkable status of the tile at the given index
	int tileType = map[yIndex, xIndex];

	bool walkable = tiles->GetIsWalkable(tileType);

	return walkable;
}

bool TileMap::GetIsLethal(int xIndex, int yIndex)
{
	//Gets the lethal status of the tile at the given index
	int tileType = map[yIndex, xIndex];

	bool lethal = tiles->GetIsLethal(tileType);

	return lethal;
}

bool TileMap::GetIsSolid(int xIndex, int yIndex)
{
	//Gets whether or not the tile at the given index is solid
	int tileType = map[yIndex, xIndex];

	bool solid = tiles->GetIsSolid(tileType);

	return solid;
}