#pragma once
#include "SpriteSheet.h"
#include "TileMap.h"
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

//Set to zero for simple hitbox based detection
#define ADVANCED_COLLISIONS 0

#define DIRECTIONS 4
#define TWO 2

	public enum EDirection
	{
		NORTH,
		EAST,
		SOUTH,
		WEST
	};

	public enum EAction
	{
		IDLE,
		WALKING,
		DYING
	};

	public enum EEdgeHitAction{
		BOUNCE,
		DIE,
		WRAP,
		STOP
	};

	public enum ESpriteCollisionAction
	{
		NONE,
		CARRY,
		KILL
	};

/*
	Author: Kierran McPherson
	Purpose: This class defines a sprite, which is an entity of the game that is animated 
			 using a collection of SpriteSheets. Sprites can move, interact with other 
			 sprites and the environment, and draw themselves to a buffer
*/
ref class Sprite
{
private:
	Graphics^ canvas;
	array<SpriteSheet^, 2>^ spriteSheets;
	Random^ random;
	EDirection currentDirection;
	EAction currentAction;
	EEdgeHitAction edgeAction;
	ESpriteCollisionAction collisionAction;
	Point location;
	Point velocity;
	array<Point>^ directionModifiers;
	Rectangle boundary;
	Rectangle hitbox;
	TileMap^ map;
	int xDeadzone;
	int yDeadzone;
	bool isAlive;
	bool isImmortal;
	bool beingCarried;

public:
	Sprite^ Next;
	
public:
	Sprite(
		array<String^, 2>^ filenames,
		array<int>^ actionFrames,
		array<int>^ actionWidth,
		array<int>^ actionHeight,
		int startXVel,
		int startYVel,
		int startXDeadZone,
		int startYDeadZone,
		bool centerSprite,
		Point startTileLocation,
		EEdgeHitAction startEdgeAction,
		ESpriteCollisionAction startCollisionAction,
		Graphics^ startCanvas,
		Random^ startRandom,
		Rectangle startBounds,
		TileMap^ startMap,
		bool startIsImmortal
		);

	void Draw();
	void Draw(Point viewportLocation);
	void Move();
	void Move(Sprite^ moveWith);
	void UpdateFrame();
	bool IsOnTile(int gameOverTile);
	bool CheckBounds(Rectangle toCheck);
	bool CheckTile(Rectangle toCheck);
	void OutOfBounds(Rectangle checkedBox);
	bool CollidesWith(Sprite^ otherGuy);
	void OnCollision(Sprite^ collided);

	Point CalculateViewportLocation(Point viewportWorldPosition);
	Point GetCenter();

	//Gets and Sets

	bool GetBeingCarried()	{return beingCarried;}
	void SetBeingCarried(bool carried)	{beingCarried = carried;}

	Rectangle GetHitBox()	{return hitbox;}
	void SetHitBox(Rectangle hb)	{hitbox = hb;}

	Point GetLocation() {return location;}
	void SetLocation(Point newLocation)	{location = newLocation;}

	Point GetVelocity() {return velocity;}
	void SetVelocity(Point newVelocity)	{velocity = newVelocity;}

	EDirection GetCurrentDirection()	{return currentDirection;}
	void SetCurrentDirection(EDirection newDirection);

	EAction GetCurrentAction()	{return currentAction;}
	void SetCurrentAction(EAction newAction);

	EEdgeHitAction GetEdgeAction()	{return edgeAction;}
	void SetEdgeAction(EEdgeHitAction e)	{edgeAction = e;}

	ESpriteCollisionAction GetCollisionAction()	{return collisionAction;}
	void SetCollisionAction(ESpriteCollisionAction e)	{collisionAction = e;}

	bool GetIsAlive() {return isAlive;}
	
	void SetBoundary(Rectangle bounds)	{boundary = bounds;}

	array<bool, 2>^ GetCollisionMap();

	Size GetFrameSize()	{return spriteSheets[currentAction, currentDirection]->GetFrameSize();}
	
};