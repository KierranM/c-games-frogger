#pragma once

//Set to 1 to show collison maps
#define SHOW_COLLISION_MAPS 0

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

/*
	Author: Kierran McPherson
	Purpose: This class defines the SpriteSheet which is a single Bitmap containing a 
			 group of frames, and can return a single frame of the sheet on request
*/
ref class SpriteSheet
{
private:
	Bitmap^ sheet;
	Bitmap^ frame;
	Graphics^ frameGraphics;
	array<bool, 2>^ collisionMap;
	int currentFrame;
	int nFrames;
	Size frameSize;
public:
	SpriteSheet(String^ filename, int startNFrames, int frameWidth, int frameHeight);
	
	Bitmap^ GetFrame();
	void UpdateCurrentFrame();
	void ResetCurrentFrame();


	//Sets and Gets
	int GetCurrentFrame()	{return currentFrame;}
	void SetCurrentFrame(int frameNum)	{currentFrame = frameNum;}

	int GetNFrames()	{return nFrames;}
	Size GetFrameSize()	{return frameSize;}

	array<bool, 2>^ GetCollisionMap()	{return collisionMap;}
	//No set for collision map
};

