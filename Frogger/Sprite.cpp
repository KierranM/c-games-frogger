#include "stdafx.h"
#include "Sprite.h"


Sprite::Sprite(
	array<String^, 2>^ filenames,
	array<int>^ actionFrames,
	array<int>^ actionWidth,
	array<int>^ actionHeight,
	int startXVel, 
	int startYVel, 
	int startXDeadZone,
	int startYDeadZone,
	bool centerSprite,
	Point startTileLocation, 
	EEdgeHitAction startEdgeAction,
	ESpriteCollisionAction startCollisionAction,
	Graphics^ startCanvas,
	Random^ startRandom,
	Rectangle startBounds,
	TileMap^ startMap,
	bool startIsImmortal
	)
{
	//Sets up the sprite using the given parameters
	random = startRandom;
	canvas = startCanvas;
	boundary = startBounds;
	currentDirection  = EDirection::NORTH;
	currentAction = EAction::IDLE;
	collisionAction = startCollisionAction;
	velocity = Point(startXVel, startYVel);
	edgeAction = startEdgeAction;
	isAlive =  true;
	isImmortal = startIsImmortal;
	map = startMap;

	//Store the sprites deadzones
	xDeadzone = startXDeadZone;
	yDeadzone = startYDeadZone;

	//Place the sprite on the correct tile
	int locX = (startTileLocation.X * map->GetTileSize()) - (xDeadzone/2);
	int locY = (startTileLocation.Y * map->GetTileSize());

	//If the sprite should be centered
	if (centerSprite)
	{
		//Center the sprite
		locY -= yDeadzone/TWO;
	}
	else
	{
		locY -= yDeadzone;
	}
	location = Point(locX, locY);

	//Set up the hitbox
	
	int hitBoxX = location.X + xDeadzone;
	int hitboxY = location.Y + yDeadzone;
	int hitboxWidth = actionWidth[currentAction] - (xDeadzone * TWO);
	int hitboxHeight = actionHeight[currentAction] - (yDeadzone * TWO);
	hitbox = Rectangle(hitBoxX,hitboxY, hitboxWidth, hitboxHeight);

	

	//Set up the directions
	directionModifiers = gcnew array<Point>(DIRECTIONS);
	directionModifiers[EDirection::NORTH] = Point(0,-1);
	directionModifiers[EDirection::EAST] = Point(1,0);
	directionModifiers[EDirection::SOUTH] = Point(0,1);
	directionModifiers[EDirection::WEST] = Point(-1,0);

	//Load the spritesheets

	//Get the number of actions that this sprite uses
	int actions = filenames->GetLength(0);

	//Create spritesheets array with the same length as filenames
	spriteSheets = gcnew array<SpriteSheet^, TWO>(actions, DIRECTIONS);

	

	/*Loop through each of this sprites actions
	  These actions must be passed in this order
		- IDLE
		- WALKING (or moving)
		- DYING
	*/
	for (int action = 0; action < actions; action++)
	{
		/* Loop though the directions. It is assumed that the spritesheets are passed in this order:
			- North
			- East
			- South
			- West
		*/
		for (int direction = 0; direction < DIRECTIONS; direction++)
		{
			spriteSheets[action, direction] = gcnew SpriteSheet(filenames[action, direction], actionFrames[action], actionWidth[action], actionHeight[action]);
		}
	}
}


void Sprite::Draw()
{
	canvas->DrawImage(spriteSheets[currentAction,currentDirection]->GetFrame(), location.X, location.Y);
}

void Sprite::Draw(Point viewportWorldLocation)
{
	//Get the tile at the center of the sprite
	Point center = GetCenter();
	int centerTileX = center.X/map->GetTileSize();
	int centerTileY = center.Y/map->GetTileSize();

	//Only draw the sprite if it is alive or on a solid block
	if(isAlive  || map->GetIsSolid(centerTileX, centerTileY))
	{
		Point spriteViewportLocation = CalculateViewportLocation(viewportWorldLocation);
		//Draws the sprite at the given location
		canvas->DrawImage(spriteSheets[currentAction,currentDirection]->GetFrame(), spriteViewportLocation.X, spriteViewportLocation.Y);

		//Display the sprites hitbox when in debug mode
		#ifdef _DEBUG
			canvas->DrawRectangle(Pens::Black, spriteViewportLocation.X + xDeadzone, spriteViewportLocation.Y + yDeadzone, hitbox.Width, hitbox.Height);
		#endif
	}
}

void Sprite::Move()
{
	if(currentAction == EAction::WALKING && currentAction != EAction::DYING)
	{
		Rectangle temp = hitbox;
		int movementX =  (velocity.X * directionModifiers[currentDirection].X);
		int movementY = (velocity.Y * directionModifiers[currentDirection].Y);

		temp.X += movementX;
		temp.Y += movementY;

		if (CheckBounds(temp))
		{
			if (CheckTile(temp))
			{
				location.X += movementX;
				location.Y += movementY;
				hitbox = temp;
			}
		}
		else
		{
			OutOfBounds(temp);
		}
	}

}

void Sprite::Move(Sprite^ moveWith)
{
	//Moves the sprite with the given sprite
	EDirection moveDirection = moveWith->GetCurrentDirection();
	Point moveVelocity = moveWith->GetVelocity();

	Rectangle temp = hitbox;
	int movementX =  (moveVelocity.X * directionModifiers[moveDirection].X);
	int movementY = (moveVelocity.Y * directionModifiers[moveDirection].Y);

	temp.X += movementX;
	temp.Y += movementY;

	if (CheckBounds(temp))
	{
		location.X += movementX;
		location.Y += movementY;
		hitbox = temp;
	}
	else
	{
		OutOfBounds(temp);
	}
}

void Sprite::OutOfBounds(Rectangle checkedBox)
{
	switch (edgeAction)
		{
		case BOUNCE:
			//Set current direction to equal itself plus half the number of directions modded by the total
			//number of directions to loop around.
			currentDirection = (EDirection)((currentDirection + (DIRECTIONS/TWO))%DIRECTIONS);
			break;
		case DIE:
			isAlive = false;
			break;
		case WRAP:
			//The sprite went off the left
			if(checkedBox.Right <= 0)
			{
				checkedBox.X = boundary.Right;
			}
			else
			{
				//The sprite went off the right
				if (checkedBox.Left >= boundary.Right)
				{
					checkedBox.X = boundary.Left - checkedBox.Width;
				}
			}
			//The sprite went off the top
			if (checkedBox.Bottom <= 0)
			{
				checkedBox.Y = boundary.Bottom;
			}
			else
			{
				if (checkedBox.Top >= boundary.Bottom)
				{
					checkedBox.Y = boundary.Top - checkedBox.Height;
				}
			}
			location.X = checkedBox.X - xDeadzone;
			location.Y = checkedBox.Y - yDeadzone;
			hitbox = checkedBox;
			break;
		case STOP:
			currentAction = EAction::IDLE;
			break;
		}
}

void Sprite::UpdateFrame()
{
	//Updates the sprites frame if the sprite is alive
	if (isAlive)
	{
		spriteSheets[currentAction, currentDirection]->UpdateCurrentFrame();
	}
	
	//If the sprite is currently in its dying animation
	if (currentAction == EAction::DYING)
	{
		//Get the number of the current frame and the total number of frames
		int curFrame = spriteSheets[currentAction, currentDirection]->GetCurrentFrame();
		int nFrames = spriteSheets[currentAction,currentDirection]->GetNFrames();

		//If the current frame is the last frame in the sequence (nFrames - 1) then set the sprite to be dead
		if (curFrame == (nFrames - 1))
		{
			isAlive = false;
		}
	}
}

bool Sprite::IsOnTile(int gameOverTile)
{
	//Checks if the tile under the sprite is the gameover tile

	//Find the tilemap location at the center of the sprite
	Point center = GetCenter();

	int tileX = center.X/map->GetTileSize();
	int tileY = center.Y/map->GetTileSize();

	int tileType = map->GetTileType(tileX, tileY);

	//Check if the sprites current tile is the same type as the game over tile
	if (tileType == gameOverTile)
	{
		//The game is over
		return true;
	}
	else
	{
		return false;
	}
}

bool Sprite::CheckBounds(Rectangle toCheck)
{
	//Check if the given rectangle is outside of the boundary rectangle

	bool inBounds = false;
	int x = toCheck.X;
	int y = toCheck.Y;
	int rightX = x + toCheck.Width;
	int bottomY = y + toCheck.Height;

	if (((x > 0) && (rightX < boundary.Right)) && ((y > 0) && (bottomY < boundary.Bottom)))
	{
		inBounds = true;
	}

	return inBounds;
}

bool Sprite::CheckTile(Rectangle toCheck)
{
	//Checks if the tile that is being moved onto is walkable
	int tileX;
	int tileY;
	bool canMove = false;

	switch (currentDirection)
	{
	case NORTH:
	case WEST:
		tileX = toCheck.X/map->GetTileSize();
		tileY = toCheck.Y/map->GetTileSize();
		break;
	case SOUTH:
	case EAST:
		tileX = (toCheck.X + hitbox.Width)/map->GetTileSize();
		tileY = (toCheck.Y + hitbox.Height)/map->GetTileSize();
		break;
	
	}
	
	//Check if the tile is walkable
	if(map->GetIsWalkable(tileX, tileY))
	{
			//The tile can be walked on
			canMove = true;
	}

	//Check if the center point of the sprite is on a lethal tile
	Point center = GetCenter();
	int centerTileX = center.X/map->GetTileSize();
	int centerTileY = center.Y/map->GetTileSize();

	//If the tile at the center of the sprite is lethal and the sprite is not immortal and is not being carried
	if (map->GetIsLethal(centerTileX, centerTileY) && !isImmortal && !beingCarried)
	{
		//Set the sprite to dying
		currentAction = EAction::DYING;
	}

	

	return canMove;
}


void Sprite::SetCurrentDirection(EDirection newDirection)
{
	//Can only change the sprites direction if the sprite is alive and not dying
	if (isAlive && (currentAction != EAction::DYING))
	{
		currentDirection = newDirection;
	}
}

void Sprite::SetCurrentAction(EAction newAction)	
{
	//Can only change the sprites action if the sprite is alive and not dying
	if (isAlive && (currentAction != EAction::DYING))
	{
		currentAction = newAction;
	}
}

Point Sprite::GetCenter()
{
	//Gets the mid point of the sprite
	Size currentFrameSize = spriteSheets[currentAction, currentDirection]->GetFrameSize();
	int x = location.X + currentFrameSize.Width/TWO;
	int y = location.Y + currentFrameSize.Height/TWO;

	return Point(x,y);
}

Point Sprite::CalculateViewportLocation(Point viewportWorldPosition)
{
	//Calculates the sprites location relevant to the given viewport location
	Point spriteViewportPosition;

	//The sprites location on the viewport is the sprites world location - the viewports world location
	spriteViewportPosition.X = location.X - viewportWorldPosition.X;
	spriteViewportPosition.Y = location.Y - viewportWorldPosition.Y;

	return spriteViewportPosition;
}

array<bool, 2>^ Sprite::GetCollisionMap()
{
	//Gets the sprites current spritesheets collisionMap
	return spriteSheets[currentAction, currentDirection]->GetCollisionMap();
}


bool Sprite::CollidesWith(Sprite^ otherGuy)
{
	//Checks if this sprite's HitBox intersects with the hitbox of the given sprite
	bool collision = false;

	//Check if the hitboxes overlap
	if (hitbox.IntersectsWith(otherGuy->GetHitBox()))
	{
		collision = true;
	}

	return collision;
}

void Sprite::OnCollision(Sprite^ collided)
{
	//Performs an action depending on the SpriteCollisionAction of the collided sprite
	ESpriteCollisionAction collidedSpriteAction = collided->GetCollisionAction();

	switch (collidedSpriteAction)
	{
	case NONE:
		//If the other sprite does nothing on collision then do nothing
		break;
	case CARRY:
		//If the other sprite carry's a sprite on collision then move this sprite with it
		beingCarried = true;
		Move(collided);
		break;
	case KILL:
		//If the other sprite kills on collision and this sprite is not immortal then set this sprite to die
		if (!isImmortal)
		{
			currentAction = EAction::DYING;
		}
		
		break;
	default:
		break;
	}
}