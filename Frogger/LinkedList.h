#pragma once
#include "Sprite.h"

/*
	Author: Kierran McPherson
	Purpose: This class defines a LinkedList for containing sprites in order
			 to make managing large collections of Sprites easier
*/

ref class LinkedList
{
private:
	Sprite^ head;
	Sprite^ tail;
public:
	LinkedList(void);
	LinkedList(Sprite^ first);

	int Count();
	void Add(Sprite^ node);
	void Remove(Sprite^ node);
	void DrawAll(Point viewportWorldPosition, Size viewportSize);
	bool InViewport(Point spriteLocation, Size spriteSize, Size viewportSize);
	void UpdateAll();
	void Clear();
	Sprite^ DetectCollision(Sprite^ otherGuy);
};
