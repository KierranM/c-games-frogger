#include "stdafx.h"
#include "GameManager.h"


GameManager::GameManager(Graphics^ startFormGraphics, Size startClientArea, Random^ startRand, Button^ startButton)
{
	//Store the button pointer
	playAgainButton = startButton;
	//Store the random pointer
	rand = startRand;

	//Store the size of the client
	clientArea = startClientArea;

	//Store the pointer to the form graphics
	formGraphics = startFormGraphics;

	//Set up the buffer
	offScreen = gcnew Bitmap(clientArea.Width, clientArea.Height);
	buffer = Graphics::FromImage(offScreen);

	//Set up the world rectangle
	int worldWidth = MAP_COLUMNS * TILE_SIZE;
	int worldHeight = MAP_ROWS * TILE_SIZE;
	world = Rectangle(0,0, worldWidth, worldHeight);

	//Set gameover to false
	gameover = false;

#pragma region Set up the land lanes
	//These are the areas that the land sprites move in
	//Note the lanes are numbers from the bottom of the screen upwards (The order the player moves into them)

	landLanes = gcnew array<Rectangle>(LAND_LANES);

	//Calculate the lane width and height (the same for all lanes)
	int laneHeightInPixels = LANE_HEIGHT * TILE_SIZE;
	int laneWidthInPixels = MAP_COLUMNS * TILE_SIZE;

	//Calculate lane one's starting position on the y axis
	int laneOneY = LAND_LANE_ONE_TILE * TILE_SIZE;

	//Create Lane one
	landLanes[0] = Rectangle(0,laneOneY, laneWidthInPixels, laneHeightInPixels);

	//Calculate lane two's starting position on the y axis
	int laneTwoY = LAND_LANE_TWO_TILE * TILE_SIZE;

	//Create Lane two
	landLanes[ONE] = Rectangle(0,laneTwoY, laneWidthInPixels, laneHeightInPixels);

	//Calculate lane three's starting position on the y axis
	int laneThreeY = LAND_LANE_THREE_TILE * TILE_SIZE;

	//Create Lane three
	landLanes[TWO] = Rectangle(0,laneThreeY, laneWidthInPixels, laneHeightInPixels);
	//End of the land lanes region
#pragma endregion

#pragma region Set up the Water Lanes
		//These are the areas that the water sprites move in
	//Note the lanes are numbers from the bottom of the screen upwards (The order the player moves into them)

	waterLanes = gcnew array<Rectangle>(WATER_LANES);

	//Calculate the lane width and height (the same for all lanes)
	int waterLaneHeightInPixels = WATER_LANE_HEIGHT * TILE_SIZE;
	int waterLaneWidthInPixels = MAP_COLUMNS * TILE_SIZE;

	//Calculate lane one's starting position on the y axis
	int waterLaneOneY = WATER_LANE_ONE_TILE * TILE_SIZE;

	//Create Lane one
	waterLanes[0] = Rectangle(0,waterLaneOneY, waterLaneWidthInPixels, waterLaneHeightInPixels);

	//Calculate lane two's starting position on the y axis
	int waterLaneTwoY = WATER_LANE_TWO_TILE * TILE_SIZE;

	//Create Lane two
	waterLanes[ONE] = Rectangle(0,waterLaneTwoY, waterLaneWidthInPixels, waterLaneHeightInPixels);

	//Calculate lane three's starting position on the y axis
	int waterLaneThreeY = WATER_LANE_THREE_TILE * TILE_SIZE;

	//Create Lane three
	waterLanes[TWO] = Rectangle(0,waterLaneThreeY, waterLaneWidthInPixels, waterLaneHeightInPixels);

	//Calculate lane fours's starting position on the y axis
	int waterLaneFourY = WATER_LANE_FOUR_TILE * TILE_SIZE;

	//Create Lane four
	waterLanes[THREE] = Rectangle(0,waterLaneFourY, waterLaneWidthInPixels, waterLaneHeightInPixels);

	//End of the water lanes region
#pragma endregion

#pragma region Set up the tilemap

	//Create the list of tiles
	TileList^ list = gcnew TileList(TILE_TYPES);

	//Create the grass tile and add it to the list
	Tile^ grass = gcnew Tile("Images/grass.bmp", true, false, true);
	list->SetTile(grass, GRASS);

	//Create the road tile and add it to the list
	Tile^ road = gcnew Tile("Images/road.bmp", true, false, true);
	list->SetTile(road, ROAD);

	//Create the water tile and add it to the list
	Tile^ water = gcnew Tile("Images/water.bmp", true, true, false);
	list->SetTile(water, WATER);

	//Create the grass to road tile and add it to the list
	Tile^ grassToRoad = gcnew Tile("Images/grass-to-road.bmp", true, false, true);
	list->SetTile(grassToRoad, GRASS_TO_ROAD);

	//Create the road to grass tile and add it to the list
	Tile^ roadToGrass = gcnew Tile("Images/road-to-grass.bmp", true, false, true);
	list->SetTile(roadToGrass, ROAD_TO_GRASS);

	//Create the grass to water tile and add it to the list
	Tile^ grassToWater = gcnew Tile("Images/grass-to-water.bmp", true, false, true);
	list->SetTile(grassToWater, GRASS_TO_WATER);

	//Create the water to cliff tile and add it to the list
	Tile^ waterToCliff = gcnew Tile("Images/water-to-cliff.bmp", true, true, false);
	list->SetTile(waterToCliff, WATER_TO_CLIFF);

	//Create the cliff to grass tile and add it to the list
	Tile^ cliffToGrass = gcnew Tile("Images/cliff-to-grass.bmp", true, false, true);
	list->SetTile(cliffToGrass, CLIFF_TO_GRASS);

	//Create the cliff inwards tile and add it to the list
	Tile^ cliffInwards = gcnew Tile("Images/cliff-inwards.bmp", true, false, true);
	list->SetTile(cliffInwards, CLIFF_INWARDS);

	//Create the water to cliff inwards tile and add it to the list
	Tile^ waterToCliffInwards = gcnew Tile("Images/water-to-cliff-inwards.bmp", true, true, false);
	list->SetTile(waterToCliffInwards, WATER_TO_CLIFF_INWARDS);

	//Create the cliff outwards tile and add it to the list
	Tile^ cliffOutwards = gcnew Tile("Images/cliff-outwards.bmp", true, false, true);
	list->SetTile(cliffOutwards, CLIFF_OUTWARDS);

	//Create the water to cliff outwards tile and add it to the list
	Tile^ waterToCliffOutwards = gcnew Tile("Images/water-to-cliff-outwards.bmp", true, true, false);
	list->SetTile(waterToCliffOutwards, WATER_TO_CLIFF_OUTWARDS);

	//Create the rabbit hole tile and add it to the list
	Tile^ rabbitHole = gcnew Tile("Images/rabbit-hole.bmp", true, false, true);
	list->SetTile(rabbitHole, RABBIT_HOLE);

	//Create the TileMap with a defined number of columns and rows, and using the created TileList
	map = gcnew TileMap(MAP_ROWS, MAP_COLUMNS, TILE_SIZE, list);
	map->LoadFromFile("frogger.csv");

	//Choose a random position on the X-axis for the rabbit hole
	int rabbitHoleTileX = rand->Next(RABBIT_HOLE_TILE_X_MIN, (MAP_COLUMNS - RABBIT_HOLE_TILE_X_MIN));

	//Set the rabbit hole tile
	map->SetTileType(rabbitHoleTileX, RABBIT_HOLE_TILE_Y, RABBIT_HOLE);
	
#pragma endregion

#pragma region Set up the viewport
	//Calculate how many tiles wide the viewport should be
	int tilesWide = clientArea.Width/TILE_SIZE;
	//Calculate how many tiles high the viewport should be
	int tilesHigh = clientArea.Height/TILE_SIZE;

	//The size in tiles of the viewport
	Size vpSize = Size(tilesWide, tilesHigh);

	//Where the viewport's starting world location should be (Single screen so 0,0)
	Point vpWorldLoc = Point(0,0);
	
	//Create the viewport
	viewPort = gcnew ViewPort(vpSize, vpWorldLoc, map, buffer);
#pragma endregion

#pragma region Set up the player character
	//Create the LinkedList of rabbits
	rabbits = gcnew LinkedList();

	//Create the array of filenames for the rabbits images
	rabbitFilenames = gcnew array<String^, 2>(RABBIT_ACTIONS,DIRECTIONS);

	//Insert the IDLE action filenames
	rabbitFilenames[EAction::IDLE, EDirection::EAST] = "Images/Rabbit/sit-east.bmp";
	rabbitFilenames[EAction::IDLE, EDirection::NORTH] = "Images/Rabbit/sit-north.bmp";
	rabbitFilenames[EAction::IDLE, EDirection::SOUTH] = "Images/Rabbit/sit-south.bmp";
	rabbitFilenames[EAction::IDLE, EDirection::WEST] = "Images/Rabbit/sit-west.bmp";

	//Insert the WALKING action filenames
	rabbitFilenames[EAction::WALKING, EDirection::EAST] = "Images/Rabbit/walk-east.bmp";
	rabbitFilenames[EAction::WALKING, EDirection::NORTH] = "Images/Rabbit/walk-north.bmp";
	rabbitFilenames[EAction::WALKING, EDirection::SOUTH] = "Images/Rabbit/walk-south.bmp";
	rabbitFilenames[EAction::WALKING, EDirection::WEST] = "Images/Rabbit/walk-west.bmp";

	//Insert the DYING action filenames
	rabbitFilenames[EAction::DYING, EDirection::EAST] = "Images/Rabbit/die-east.bmp";
	rabbitFilenames[EAction::DYING, EDirection::NORTH] = "Images/Rabbit/die-north.bmp";
	rabbitFilenames[EAction::DYING, EDirection::SOUTH] = "Images/Rabbit/die-south.bmp";
	rabbitFilenames[EAction::DYING, EDirection::WEST] = "Images/Rabbit/die-west.bmp";

	//Set up the array containing the number of frames for each action
	rabbitFrames = gcnew array<int>(RABBIT_ACTIONS);

	rabbitFrames[EAction::IDLE] = RABBIT_IDLE_FRAMES;
	rabbitFrames[EAction::WALKING] = RABBIT_WALKING_FRAMES;
	rabbitFrames[EAction::DYING] = RABBIT_DYING_FRAMES;

	//Set up the array containing the frame heights for each action
	rabbitFrameHeight = gcnew array<int>(RABBIT_ACTIONS);

	rabbitFrameHeight[EAction::IDLE] = RABBIT_HEIGHT;
	rabbitFrameHeight[EAction::WALKING] = RABBIT_HEIGHT;
	rabbitFrameHeight[EAction::DYING] = RABBIT_HEIGHT;

	//Set up the array containing the frame widths for each action
	rabbitFrameWidth = gcnew array<int>(RABBIT_ACTIONS);

	rabbitFrameWidth[EAction::IDLE] = RABBIT_WIDTH;
	rabbitFrameWidth[EAction::WALKING] = RABBIT_WIDTH;
	rabbitFrameWidth[EAction::DYING] = RABBIT_WIDTH;

	CreateRabbit();
#pragma endregion

#pragma region Set up the enemy land sprites

#pragma region Set up the two horse sprites

	//Create the array of filenames for the light horse images
	array<String^, 2>^ lightHorseFilenames = gcnew array<String^, 2>(HORSE_ACTIONS,DIRECTIONS);

	//Insert the IDLE action filenames
	lightHorseFilenames[EAction::IDLE, EDirection::EAST] = "Images/LightHorse/stop-east.bmp";
	lightHorseFilenames[EAction::IDLE, EDirection::NORTH] = "Images/LightHorse/stop-north.bmp";
	lightHorseFilenames[EAction::IDLE, EDirection::SOUTH] = "Images/LightHorse/stop-south.bmp";
	lightHorseFilenames[EAction::IDLE, EDirection::WEST] = "Images/LightHorse/stop-west.bmp";

	//Insert the WALKING action filenames
	lightHorseFilenames[EAction::WALKING, EDirection::EAST] = "Images/LightHorse/walk-east.bmp";
	lightHorseFilenames[EAction::WALKING, EDirection::NORTH] = "Images/LightHorse/walk-north.bmp";
	lightHorseFilenames[EAction::WALKING, EDirection::SOUTH] = "Images/LightHorse/walk-south.bmp";
	lightHorseFilenames[EAction::WALKING, EDirection::WEST] = "Images/LightHorse/walk-west.bmp";


	//Create the array of filenames for the dark horse images
	array<String^, 2>^ darkHorseFilenames = gcnew array<String^, 2>(HORSE_ACTIONS,DIRECTIONS);

	//Insert the IDLE action filenames
	darkHorseFilenames[EAction::IDLE, EDirection::EAST] = "Images/DarkHorse/stop-east.bmp";
	darkHorseFilenames[EAction::IDLE, EDirection::NORTH] = "Images/DarkHorse/stop-north.bmp";
	darkHorseFilenames[EAction::IDLE, EDirection::SOUTH] = "Images/DarkHorse/stop-south.bmp";
	darkHorseFilenames[EAction::IDLE, EDirection::WEST] = "Images/DarkHorse/stop-west.bmp";

	//Insert the WALKING action filenames
	darkHorseFilenames[EAction::WALKING, EDirection::EAST] = "Images/DarkHorse/run-east.bmp";
	darkHorseFilenames[EAction::WALKING, EDirection::NORTH] = "Images/DarkHorse/run-north.bmp";
	darkHorseFilenames[EAction::WALKING, EDirection::SOUTH] = "Images/DarkHorse/run-south.bmp";
	darkHorseFilenames[EAction::WALKING, EDirection::WEST] = "Images/DarkHorse/run-west.bmp";

	//Set up the array containing the number of frames for each action
	array<int>^ horseFrames = gcnew array<int>(HORSE_ACTIONS);

	horseFrames[EAction::IDLE] = HORSE_IDLE_FRAMES;
	horseFrames[EAction::WALKING] = HORSE_MOVING_FRAMES;

	//Set up the array containing the frame heights for each action
	array<int>^ horseFrameHeight = gcnew array<int>(HORSE_ACTIONS);

	horseFrameHeight[EAction::IDLE] = HORSE_HEIGHT;
	horseFrameHeight[EAction::WALKING] = HORSE_HEIGHT;

	//Set up the array containing the frame widths for each action
	array<int>^ horseFrameWidth = gcnew array<int>(HORSE_ACTIONS);

	horseFrameWidth[EAction::IDLE] = HORSE_WIDTH;
	horseFrameWidth[EAction::WALKING] = HORSE_WIDTH;

//End of horse sprites region
#pragma endregion

#pragma region Set up the wagon sprite
	//Create the array of filenames for the wagon images
	array<String^, 2>^ wagonFilenames = gcnew array<String^, 2>(WAGON_ACTIONS,DIRECTIONS);

	//Insert the IDLE action filenames
	wagonFilenames[EAction::IDLE, EDirection::EAST] = "Images/Wagon/stop-east.bmp";
	wagonFilenames[EAction::IDLE, EDirection::NORTH] = "Images/Wagon/stop-north.bmp";
	wagonFilenames[EAction::IDLE, EDirection::SOUTH] = "Images/Wagon/stop-south.bmp";
	wagonFilenames[EAction::IDLE, EDirection::WEST] = "Images/Wagon/stop-west.bmp";

	//Insert the WALKING action filenames
	wagonFilenames[EAction::WALKING, EDirection::EAST] = "Images/Wagon/drive-east.bmp";
	wagonFilenames[EAction::WALKING, EDirection::NORTH] = "Images/Wagon/drive-north.bmp";
	wagonFilenames[EAction::WALKING, EDirection::SOUTH] = "Images/Wagon/drive-south.bmp";
	wagonFilenames[EAction::WALKING, EDirection::WEST] = "Images/Wagon/drive-west.bmp";

	//Set up the array containing the number of frames for each action
	array<int>^ wagonFrames = gcnew array<int>(WAGON_ACTIONS);

	wagonFrames[EAction::IDLE] = WAGON_IDLE_FRAMES;
	wagonFrames[EAction::WALKING] = WAGON_MOVING_FRAMES;

	//Set up the array containing the frame heights for each action
	array<int>^ wagonFrameHeight = gcnew array<int>(WAGON_ACTIONS);

	wagonFrameHeight[EAction::IDLE] = WAGON_HEIGHT;
	wagonFrameHeight[EAction::WALKING] = WAGON_HEIGHT;

	//Set up the array containing the frame widths for each action
	array<int>^ wagonFrameWidth = gcnew array<int>(WAGON_ACTIONS);

	wagonFrameWidth[EAction::IDLE] = WAGON_WIDTH;
	wagonFrameWidth[EAction::WALKING] = WAGON_WIDTH;
//end of wagon sprite
#pragma endregion 

#pragma region Create the land sprites
	//Create the LinkedList of land NPCs
	landNPCS = gcnew LinkedList();

	//Create the horses in Lane one
	int startX = rand->Next(LANE_START_TILE_MIN, LANE_START_TILE_MAX);
	CreateLaneSprites(startX, LAND_LANE_ONE_TILE, LAND_LANE_ONE_HORSES, landLanes[0], landNPCS, lightHorseFilenames, horseFrames, horseFrameWidth, horseFrameHeight, LIGHT_HORSE_SPEED, HORSE_DEADZONE_X, HORSE_DEADZONE_Y, false, EEdgeHitAction::WRAP, ESpriteCollisionAction::KILL, EDirection::EAST);

	//Create the horses in lane two
	startX = rand->Next(LANE_START_TILE_MIN, LANE_START_TILE_MAX);
	CreateLaneSprites(startX, LAND_LANE_TWO_TILE, LAND_LANE_TWO_HORSES, landLanes[ONE], landNPCS, darkHorseFilenames, horseFrames, horseFrameWidth, horseFrameHeight, DARK_HORSE_SPEED, HORSE_DEADZONE_X, HORSE_DEADZONE_Y, true, EEdgeHitAction::WRAP, ESpriteCollisionAction::KILL, EDirection::WEST);

	
	//Create the wagons in lane three
	startX = rand->Next(LANE_START_TILE_MIN, LANE_START_TILE_MAX);
	CreateLaneSprites(startX, LAND_LANE_THREE_TILE, LAND_LANE_THREE_WAGONS, landLanes[TWO], landNPCS, wagonFilenames, wagonFrames, wagonFrameWidth, wagonFrameHeight, WAGON_SPEED, WAGON_DEADZONE_X, WAGON_DEADZONE_Y, false, EEdgeHitAction::WRAP, ESpriteCollisionAction::KILL, EDirection::EAST);
	//End of creating land sprites region
#pragma endregion
	
//End of Enemy land sprites
#pragma endregion 

#pragma region Set up the ally water sprites

	//Set up the array of log filenames
	array<String^, 2>^ logFilenames = gcnew array<String^, 2>(LOG_ACTIONS, DIRECTIONS);

	logFilenames[EAction::IDLE, EDirection::NORTH] = "Images/Logs/logs-north.bmp";
	logFilenames[EAction::IDLE, EDirection::SOUTH] = "Images/Logs/logs-south.bmp";
	logFilenames[EAction::IDLE, EDirection::EAST] = "Images/Logs/logs-east.bmp";
	logFilenames[EAction::IDLE, EDirection::WEST] = "Images/Logs/logs-west.bmp";

	logFilenames[EAction::WALKING, EDirection::NORTH] = "Images/Logs/logs-north.bmp";
	logFilenames[EAction::WALKING, EDirection::SOUTH] = "Images/Logs/logs-south.bmp";
	logFilenames[EAction::WALKING, EDirection::EAST] = "Images/Logs/logs-east.bmp";
	logFilenames[EAction::WALKING, EDirection::WEST] = "Images/Logs/logs-west.bmp";

	//Set up the array with the number of frames for each action
	array<int>^ logFrames = gcnew array<int>(LOG_ACTIONS);

	logFrames[EAction::IDLE] = LOG_FRAMES;
	logFrames[EAction::WALKING] = LOG_FRAMES;

	//Set up the array with the width of a frame for each action
	array<int>^ logFrameWidth = gcnew array<int>(LOG_ACTIONS);

	logFrameWidth[EAction::IDLE] = LOG_WIDTH;
	logFrameWidth[EAction::WALKING] = LOG_WIDTH;

	//Set up the array with the height of a frame for each action
	array<int>^ logFrameHeight = gcnew array<int>(LOG_ACTIONS);

	logFrameHeight[EAction::IDLE] = LOG_HEIGHT;
	logFrameHeight[EAction::WALKING] = LOG_HEIGHT;

	waterNPCS = gcnew LinkedList();

	//Create the logs in Lane one
	int waterStartX = rand->Next(LANE_START_TILE_MIN, LANE_START_TILE_MAX);
	CreateLaneSprites(waterStartX, WATER_LANE_ONE_TILE, WATER_LANE_ONE_LOGS, waterLanes[0], waterNPCS, logFilenames, logFrames, logFrameWidth, logFrameHeight, MEDIUM_LOG_SPEED, LOG_DEADZONE_X, LOG_DEADZONE_Y, false, EEdgeHitAction::WRAP, ESpriteCollisionAction::CARRY, EDirection::EAST);
	
	//Create the logs in Lane two
	waterStartX = rand->Next(LANE_START_TILE_MIN, LANE_START_TILE_MAX);
	CreateLaneSprites(waterStartX, WATER_LANE_TWO_TILE, WATER_LANE_TWO_LOGS, waterLanes[ONE], waterNPCS, logFilenames, logFrames, logFrameWidth, logFrameHeight, SLOW_LOG_SPEED, LOG_DEADZONE_X, LOG_DEADZONE_Y, false, EEdgeHitAction::WRAP, ESpriteCollisionAction::CARRY, EDirection::WEST);

	//Create the logs in Lane three
	waterStartX = rand->Next(LANE_START_TILE_MIN, LANE_START_TILE_MAX);
	CreateLaneSprites(waterStartX, WATER_LANE_THREE_TILE, WATER_LANE_THREE_LOGS, waterLanes[TWO], waterNPCS, logFilenames, logFrames, logFrameWidth, logFrameHeight, FAST_LOG_SPEED, LOG_DEADZONE_X, LOG_DEADZONE_Y, false, EEdgeHitAction::WRAP, ESpriteCollisionAction::CARRY, EDirection::EAST);

	//Create the logs in Lane four
	waterStartX = rand->Next(LANE_START_TILE_MIN, LANE_START_TILE_MAX);
	CreateLaneSprites(waterStartX, WATER_LANE_FOUR_TILE, WATER_LANE_FOUR_LOGS, waterLanes[THREE], waterNPCS, logFilenames, logFrames, logFrameWidth, logFrameHeight, MEDIUM_LOG_SPEED, LOG_DEADZONE_X, LOG_DEADZONE_Y, false, EEdgeHitAction::WRAP, ESpriteCollisionAction::CARRY, EDirection::WEST);
	//End of the allied water sprites region
#pragma endregion

}

void GameManager::CreateLaneSprites(int tileX, int tileY, int numSprites, Rectangle lane, LinkedList^ listToAdd, array<String^,2>^ filenames, array<int>^ numberOfFrames, array<int>^ frameWidth, array<int>^ frameHeight, int speed, int deadzoneX, int deadzoneY, bool center, EEdgeHitAction edgeAction, ESpriteCollisionAction collisionAction, EDirection direction)
{
	//Sets up a lane of sprites
	for (int i = 0; i < numSprites; i++)
	{
		Point startLocation = Point(tileX,  tileY);
		Sprite^ sprite = gcnew Sprite(filenames, numberOfFrames, frameWidth, frameHeight, speed, speed, deadzoneX, deadzoneY, center, startLocation, edgeAction, collisionAction, buffer, rand, lane, map, true);
		sprite->SetCurrentAction(EAction::WALKING);
		sprite->SetCurrentDirection(direction);
		//add the nPC to the list
		listToAdd->Add(sprite);
		
		//Evenly space the sprites within the lane
		tileX += (((lane.Width + frameWidth[0])/numSprites)/TILE_SIZE);
	}
}

void GameManager::CreateRabbit()
{
	//Generate a new rabbit
	int startX = rand->Next(ONE, MAP_COLUMNS - ONE);
	Point startLoc = Point(startX, RABBIT_START_LOCATION_Y);

	rabbit = gcnew Sprite(rabbitFilenames, rabbitFrames, rabbitFrameWidth, rabbitFrameHeight, RABBIT_SPEED, RABBIT_SPEED, RABBIT_DEADZONE_X, RABBIT_DEADZONE_Y, true, startLoc, EEdgeHitAction::STOP, ESpriteCollisionAction::NONE, buffer, rand, world, map, false);
	rabbits->Add(rabbit);
}

void GameManager::Reset()
{
	gameover = false;
	rabbits->Clear();
	
	CreateRabbit();

	for (int i = 0; i < MAP_COLUMNS; i++)
	{
		map->SetTileType(i, RABBIT_HOLE_TILE_Y, GRASS);
	}

	//Choose a random position on the X-axis for the rabbit hole
	int rabbitHoleTileX = rand->Next(RABBIT_HOLE_TILE_X_MIN, (MAP_COLUMNS - RABBIT_HOLE_TILE_X_MIN));

	//Set the rabbit hole tile
	map->SetTileType(rabbitHoleTileX, RABBIT_HOLE_TILE_Y, RABBIT_HOLE);

	playAgainButton->Visible = false;
	playAgainButton->Enabled = false;
}

void GameManager::Tick()
{
	//If the game is not over then draw all the game entities
	if (!gameover)
	{
		//Move all the game entities
		rabbit->Move();
		landNPCS->UpdateAll();
		waterNPCS->UpdateAll();

		//Check if the rabbit is over the rabbit hole
		if (rabbit->IsOnTile(RABBIT_HOLE))
		{
			gameover = true;
		}

		//Check if any land sprites are colliding with the rabbit
		Sprite^ collided;

		collided = landNPCS->DetectCollision(rabbit);

		//If collided is a Sprite (instead of nullptr) then do rabbits collision action
		if (collided != nullptr)
		{
			rabbit->OnCollision(collided);
		}

		//Check if any water sprites are colliding with the rabbit
		collided = waterNPCS->DetectCollision(rabbit);

		//If collided is a Sprite (instead of nullptr) then do rabbits collision action
		if (collided != nullptr)
		{
			rabbit->OnCollision(collided);
		}
		else
		{
			//set the rabbits being carried to false
			rabbit->SetBeingCarried(false);
			//Force a check of the rabbits current tile
			rabbit->CheckTile(rabbit->GetHitBox());
		}

		//Check if the rabbit is dead
		if (rabbit->GetIsAlive() == false)
		{
			CreateRabbit();
		}

		//Update the sprites frames
		rabbit->UpdateFrame();

		//Draw all the game entities
		viewPort->Draw();

		//Draw the water sprites before the rabbit
		waterNPCS->DrawAll(viewPort->GetWorldLocation(), viewPort->GetSizeInPixels());

		//Draw all the rabbits
		rabbits->DrawAll(viewPort->GetWorldLocation(), viewPort->GetSizeInPixels());

		//Draw the land sprites after the rabbit
		landNPCS->DrawAll(viewPort->GetWorldLocation(), viewPort->GetSizeInPixels());

		//If running in debug configuration then display the lanes as rectangles on the screen
#ifdef _DEBUG
		for each (Rectangle rect in landLanes)
		{
			buffer->DrawRectangle(Pens::Black, rect);
		}

		for each (Rectangle rect in waterLanes)
		{
			buffer->DrawRectangle(Pens::Black, rect);
		}
#endif 	

	//Draw the number of rabbits killed so far to the screen
	int rabbitsKilled = rabbits->Count() - ONE; // -1 to remove the living rabbit from the count
	Drawing::Font^ score = gcnew Drawing::Font("Arial", GAME_FONT_SIZE);
	//Draw the string to the buffer
	buffer->DrawString("Rabbits: " + Convert::ToString(rabbitsKilled), score,Brushes::White, 0,0);

	}
	else //The game is over
	{
		int rabbitsKilled = rabbits->Count() - ONE; // -1 to remove the living rabbit from the count

		//The Y location of the message should be in the middle of the screen
		int messageLocY = (offScreen->Height/TWO) - (GAME_FONT_SIZE/TWO);
		String^ message = "You slaughtered " + Convert::ToString(rabbitsKilled) + " bunnies to get to the hole";
		Drawing::Font^ win = gcnew Drawing::Font("Arial", GAME_FONT_SIZE);
		buffer->DrawString(message, win, Brushes::White, WIN_MESSAGE_X, messageLocY);

		playAgainButton->Visible = true;
		playAgainButton->Enabled = true;
	}
	


	//Write the acknowledgements to the bottom
	Drawing::Font^ ack = gcnew Drawing::Font("Arial", SMALL_FONT);
	int ackYLoc = (AKNOWLEDGEMENT_Y_TILE * TILE_SIZE) + SMALL_FONT;
	buffer->DrawString("Sprites from reinerstilesets.de", ack,Brushes::White, AKNOWLEDGEMENT_ONE_X,ackYLoc);
	buffer->DrawString("Tiles thanks to Jetrel and Redshrike", ack,Brushes::White, AKNOWLEDGEMENT_TWO_X, ackYLoc);

	//Draw the buffer to the screen
	formGraphics->DrawImage(offScreen, 0, 0);
}

void GameManager::OnKeyDown(Keys e)
{
	rabbit->SetCurrentAction(EAction::WALKING);
	switch (e)
	{
	case Keys::Left:
		rabbit->SetCurrentDirection(EDirection::WEST);
		break;
	case Keys::Right:
		rabbit->SetCurrentDirection(EDirection::EAST);
		break;
	case Keys::Up:
		rabbit->SetCurrentDirection(EDirection::NORTH);
		break;
	case Keys::Down:
		rabbit->SetCurrentDirection(EDirection::SOUTH);
		break;
	case Keys::Escape:
		Application::Exit();
		break;
	default:
		break;
	}
}

void GameManager::OnKeyUp(Keys e)
{
	rabbit->SetCurrentAction(EAction::IDLE);
}
