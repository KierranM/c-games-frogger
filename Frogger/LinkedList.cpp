#include "StdAfx.h"
#include "LinkedList.h"


LinkedList::LinkedList(void)
{
	//Make both pointers point to nothing
	head = nullptr;
	tail = nullptr;
}


LinkedList::LinkedList(Sprite^ first){
	//Set both pointers to point to the first pellet
	head = first;
	tail = first;
}


int LinkedList::Count(){
	//Loops through the list and counts the number of sprites
	int count = 0;
	Sprite^ nodeWalker = head;
	while(nodeWalker != nullptr){
		nodeWalker = nodeWalker->Next;
		count++;
	}

	return count;

}


void LinkedList::Add(Sprite ^node){
	if(tail == nullptr){ //Empty list
		head = node;
		tail = node;
	}
	else{ //Adding to end
		tail->Next = node;
		tail = node;
	}
}


void LinkedList::Remove(Sprite ^node){
	Sprite^ nodeWalker;

	//start at the beginning of the list
	nodeWalker = head;

	//If the node to be deleted is the head
	if(node == head){
		//If the node is the head and has no next
		if(node->Next == nullptr){
			head = nullptr;
			tail = nullptr;
		}
		else{ //Node is the first but not the only item in the list
			head = head->Next;
		}
	}
	else{ //The node to be deleted are not head
		Sprite^ temp;
		while(nodeWalker->Next != node){
			nodeWalker = nodeWalker->Next;
		}
		//Store the node
		temp = nodeWalker->Next;
		//Check if the node that is being removed is the tail
		if(temp == tail){
			//Make the tail the current node and set its next to nothing
			tail = nodeWalker;
			nodeWalker->Next = nullptr;
		}
		else{
			//Otherwise make the list skip over the node that is being removed
			nodeWalker->Next = temp->Next;
		}

	}
}


void LinkedList::DrawAll(Point viewportWorldPosition, Size viewportSize){
	//Loops through all sprites in the list and tells them to draw themselves if they are within the viewport area
	Sprite^ nodeWalker = head;

	if(nodeWalker != nullptr){

		while(nodeWalker != nullptr){

			

			//Get the sprites location relevant to the viewport
			Point spriteViewportPosition = nodeWalker->CalculateViewportLocation(viewportWorldPosition);

			//Get the size of the sprites current frame
			Size spriteSize = nodeWalker->GetFrameSize();

			//If the sprite is within the viewport
			if (InViewport(spriteViewportPosition, spriteSize, viewportSize))
			{
				nodeWalker->Draw(viewportWorldPosition);
			}

			nodeWalker = nodeWalker->Next;
		}
	}
}

bool LinkedList::InViewport(Point spriteLocation, Size spriteSize, Size viewportSize)
{
	//Checks if the given coordinates are within the bounds of the viewport
	if ((spriteLocation.X + spriteSize.Width) >= 0)
	{
		if (spriteLocation.X <= (viewportSize.Width))
		{
			if ((spriteLocation.Y + spriteSize.Height) >= 0)
			{
				if (spriteLocation.Y <= (viewportSize.Height))
				{
					return true;
				}
			}
		}
	}

	return false;
}


void LinkedList::UpdateAll(){
	//Loops through all sprites in the list and first moves the pellet and then
	//checks if they are out of bounds and then removes them if they are
	Sprite^ nodeWalker = head;
	if(nodeWalker != nullptr){
		while(nodeWalker != nullptr){
			nodeWalker->Move();
			nodeWalker->UpdateFrame();
			Sprite^ temp = nodeWalker;
			if(nodeWalker->GetIsAlive() == false){
				Remove(nodeWalker);
			}
			nodeWalker = temp->Next;
		}
	}
}

Sprite^ LinkedList::DetectCollision(Sprite^ otherGuy)
{
	//Loop through the list of sprites and return the first sprite that collides with the given sprite (or null)
	
	Sprite^ nodeWalker = head;
	if(nodeWalker != nullptr){
		while(nodeWalker != nullptr){
			//Check if nodewalker is colliding with the given sprite
			if (nodeWalker->CollidesWith(otherGuy))
			{
				//Return nodewalker
				return nodeWalker;
			}
			nodeWalker = nodeWalker->Next;
		}
	}

	//If it reaches this point then no sprite collided with otherGuy so return nullptr
	return nullptr;
}

void LinkedList::Clear()
{
	//Clears the linked list by resetting the head and tail pointers
	head = nullptr;
	tail = nullptr;
}