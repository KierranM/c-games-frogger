#pragma once
#include "GameManager.h"


#define SCREEN_WIDTH 1024
#define SCREEN_HEIGHT 768
#define SCREEN_START_X 0
#define SCREEN_START_Y 0
#define TIMER_INTERVAL 50

namespace Frogger {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

/*
	Author: Kierran McPherson
	Purpose: This class defines the form, much of which is automatically generated,
			 it creates the instance of GameManager and then passes all input to the manager
*/
	public ref class Form1 : public System::Windows::Forms::Form
	{
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Timer^  gameTimer;
	private: System::Windows::Forms::Label^  instructions;
	private: System::Windows::Forms::Button^  startButton;
	private: System::Windows::Forms::Button^  playAgainButton;


	protected: 
	private: System::ComponentModel::IContainer^  components;

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Form1::typeid));
			this->gameTimer = (gcnew System::Windows::Forms::Timer(this->components));
			this->instructions = (gcnew System::Windows::Forms::Label());
			this->startButton = (gcnew System::Windows::Forms::Button());
			this->playAgainButton = (gcnew System::Windows::Forms::Button());
			this->SuspendLayout();
			// 
			// gameTimer
			// 
			this->gameTimer->Tick += gcnew System::EventHandler(this, &Form1::gameTimer_Tick);
			// 
			// instructions
			// 
			this->instructions->AutoSize = true;
			this->instructions->Font = (gcnew System::Drawing::Font(L"Arial Narrow", 21.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->instructions->ForeColor = System::Drawing::Color::White;
			this->instructions->Location = System::Drawing::Point(163, 193);
			this->instructions->Name = L"instructions";
			this->instructions->Size = System::Drawing::Size(624, 231);
			this->instructions->TabIndex = 0;
			this->instructions->Text = L"Get to the rabbit hole, whatever the cost\r\n\r\nBeware the beasts on the road and th" 
				L"e treacherous water\r\n\r\nClick the button below to begin\r\n\r\nHit Esc at any time to" 
				L" quit";
			// 
			// startButton
			// 
			this->startButton->BackColor = System::Drawing::Color::Black;
			this->startButton->BackgroundImage = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"startButton.BackgroundImage")));
			this->startButton->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Center;
			this->startButton->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->startButton->Font = (gcnew System::Drawing::Font(L"Arial Narrow", 21.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->startButton->Location = System::Drawing::Point(352, 444);
			this->startButton->Name = L"startButton";
			this->startButton->Size = System::Drawing::Size(246, 74);
			this->startButton->TabIndex = 1;
			this->startButton->UseVisualStyleBackColor = false;
			this->startButton->Click += gcnew System::EventHandler(this, &Form1::startButton_Click);
			this->startButton->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &Form1::Form1_KeyDown);
			this->startButton->MouseEnter += gcnew System::EventHandler(this, &Form1::startButton_MouseEnter);
			this->startButton->MouseLeave += gcnew System::EventHandler(this, &Form1::startButton_MouseLeave);
			// 
			// playAgainButton
			// 
			this->playAgainButton->BackColor = System::Drawing::Color::Black;
			this->playAgainButton->BackgroundImage = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"playAgainButton.BackgroundImage")));
			this->playAgainButton->BackgroundImageLayout = System::Windows::Forms::ImageLayout::Center;
			this->playAgainButton->Enabled = false;
			this->playAgainButton->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->playAgainButton->Font = (gcnew System::Drawing::Font(L"Arial Narrow", 21.75F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->playAgainButton->Location = System::Drawing::Point(352, 444);
			this->playAgainButton->Name = L"playAgainButton";
			this->playAgainButton->Size = System::Drawing::Size(246, 74);
			this->playAgainButton->TabIndex = 2;
			this->playAgainButton->UseVisualStyleBackColor = false;
			this->playAgainButton->Visible = false;
			this->playAgainButton->Click += gcnew System::EventHandler(this, &Form1::playAgainButton_Click);
			this->playAgainButton->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &Form1::Form1_KeyDown);
			this->playAgainButton->MouseEnter += gcnew System::EventHandler(this, &Form1::playAgainButton_MouseEnter);
			this->playAgainButton->MouseLeave += gcnew System::EventHandler(this, &Form1::playAgainButton_MouseLeave);
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::Color::Black;
			this->ClientSize = System::Drawing::Size(1008, 729);
			this->Controls->Add(this->playAgainButton);
			this->Controls->Add(this->startButton);
			this->Controls->Add(this->instructions);
			this->DoubleBuffered = true;
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::None;
			this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
			this->MaximizeBox = false;
			this->MinimizeBox = false;
			this->Name = L"Form1";
			this->StartPosition = System::Windows::Forms::FormStartPosition::Manual;
			this->Text = L"Frogger";
			this->Load += gcnew System::EventHandler(this, &Form1::Form1_Load);
			this->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &Form1::Form1_KeyDown);
			this->KeyUp += gcnew System::Windows::Forms::KeyEventHandler(this, &Form1::Form1_KeyUp);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

#pragma region Form Variables

	private:
		GameManager^ game;
		Graphics^ formGraphics;
		Random^ rand;
		Bitmap^ start;
		Bitmap^ startHighlighted;
		Bitmap^ playAgain;
		Bitmap^ playAgainHighlighted;

#pragma endregion
	private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {
				 //Set the screen width, height, and location
				 Width = SCREEN_WIDTH;
				 Height = SCREEN_HEIGHT;
				 Location.X = SCREEN_START_X;
				 Location.Y = SCREEN_START_Y;

				 //Load the start button bitmaps
				 start = gcnew Bitmap("Images/play.png");
				 startHighlighted = gcnew Bitmap("Images/play-highlighted.png");
				 playAgain = gcnew Bitmap("Images/play-again.png");
				 playAgainHighlighted = gcnew Bitmap("Images/play-again-highlighted.png");

				 //Get the form graphics
				 formGraphics = CreateGraphics();
				 
				 //Create a random instance
				 rand = gcnew Random();
				 
				 //Set up the GameManager
				 game = gcnew GameManager(formGraphics, ClientSize, rand, playAgainButton);

				 //Set the timer interval
				 gameTimer->Interval = TIMER_INTERVAL;
			 }

	private: System::Void gameTimer_Tick(System::Object^  sender, System::EventArgs^  e) {
				 game->Tick();
			 }

	private: System::Void Form1_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) {
				 //Send the KeyCode to the GameManager
				 game->OnKeyDown(e->KeyCode);
			 }

	private: System::Void Form1_KeyUp(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) {
				 //Send the KeyCode to the GameManager
				 game->OnKeyUp(e->KeyCode);
			 }

	private: System::Void startButton_MouseEnter(System::Object^  sender, System::EventArgs^  e) {
				 //Light up the button
				 startButton->BackgroundImage = startHighlighted;
			 }

	private: System::Void startButton_MouseLeave(System::Object^  sender, System::EventArgs^  e) {
				 //Dim the button
				 startButton->BackgroundImage = start;
			}

	private: System::Void playAgainButton_MouseEnter(System::Object^  sender, System::EventArgs^  e) {
				//Light up the button
				 playAgainButton->BackgroundImage = playAgainHighlighted;
			}

	private: System::Void playAgainButton_MouseLeave(System::Object^  sender, System::EventArgs^  e) {
				//Dim up the button
				 playAgainButton->BackgroundImage = playAgain;
			 }

	private: System::Void playAgainButton_Click(System::Object^  sender, System::EventArgs^  e) {
				 game->Reset();
			}

	private: System::Void startButton_Click(System::Object^  sender, System::EventArgs^  e) {
				 //Disable and hide the start button and instruction label
				 instructions->Visible = false;
				 instructions->Enabled = false;

				 startButton->Visible = false;
				 startButton->Enabled = false;

				 //Set up the timer
				 gameTimer->Enabled = true;
		 }


};
}

