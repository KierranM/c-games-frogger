#pragma once
#include "Tile.h"
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

/*
	Author: Kierran McPherson
	Purpose: This class defines the tile list, which is an array of all the tiles 
			 in the game indexed by the tile type
*/
ref class TileList
{
private:
	array<Tile^>^ tiles;
public:
	TileList(int numberOfTiles);

	Tile^ GetTile(int index)	{return tiles[index];}
	void SetTile(Tile^ tile, int index)	{tiles[index] = tile;}

	bool GetIsWalkable(int tileType)	{return tiles[tileType]->GetIsWalkable();}
	bool GetIsLethal(int tileType)	{return tiles[tileType]->GetIsLethal();}
	bool GetIsSolid(int tileType)	{return tiles[tileType]->GetIsSolid();}
};

