#pragma once
#include "TileList.h"
	using namespace System;
	using namespace System::IO;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

/*
	Author: Kierran McPherson
	Purpose: This class defines a TileMap which stores a map of the types of 
			 tiles in a 2-dimensional array. The tilemap can be loaded from a file
*/
ref class TileMap
{
private:
	array<int, 2>^ map;
	int rows;
	int columns;
	int tileSize;
	TileList^ tiles;

public:
	TileMap(int startRows, int startCols, int startTileSize, TileList^ startList);

	Bitmap^ GetMapCellBitmap(int c, int r);
	void LoadFromFile(String^ filename);

	int GetRows()	{return rows;}
	int GetColumns()	{return columns;}
	Tile^ GetTile(int xIndex, int yIndex);
	bool GetIsWalkable(int xIndex, int yIndex);
	bool GetIsLethal(int xIndex, int yIndex);
	bool GetIsSolid(int xIndex, int yIndex);

	int GetWidth()	{return columns * tileSize;}
	int GetHeight()	{return rows * tileSize;}
	int GetTileSize()	{return tileSize;}
	
	int GetTileType(int xIndex, int yIndex)	{return map[yIndex, xIndex];}
	void SetTileType(int xIndex, int yIndex, int type) {map[yIndex, xIndex] = type;}
};

