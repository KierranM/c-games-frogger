#pragma once
#include "TileMap.h"
#include "Sprite.h"
#define TWO 2

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

/*
	Author: Kierran McPherson
	Purpose: This class defines the Viewport which is a viewable area of the world
			 All sprites to be drawn are drawn relative to the viewport
*/

ref class ViewPort
{
private:
	Size sizeInTiles;
	Size sizeInPixels;
	Point worldLocation;
	TileMap^ map;
	Graphics^ canvas;

public:
	ViewPort(
		Size startSizeInTiles,
		Point startWorldLocation,
		TileMap^ startMap,
		Graphics^ startCanvas
		);

	void Draw();
	void Move(int x, int y);
	void CenterOn(Sprite^ focused);
	bool CheckBoundsX(int xToCheck);
	bool CheckBoundsY(int yToCheck);
	
	Point GetWorldLocation()	{return worldLocation;}
	Size GetSizeInTiles()	{return sizeInTiles;}
	Size GetSizeInPixels()	{return sizeInPixels;}
};

