#pragma once
#include "ViewPort.h"
#include "TileMap.h"
#include "LinkedList.h"

//Generic numerical constants
#define ONE 1
#define TWO 2
#define THREE 3
#define FOUR 4

//Constants for the game
#define TILE_TYPES 13
#define MAP_ROWS 24
#define MAP_COLUMNS 32
#define TILE_SIZE 32
#define RABBIT_HOLE_TILE_Y 1
#define RABBIT_HOLE_TILE_X_MIN 2
#define LAND_LANES 3
#define LANE_HEIGHT 3
#define LANE_START_TILE_MIN 1
#define LANE_START_TILE_MAX 6
#define LAND_LANE_ONE_TILE 19
#define LAND_LANE_TWO_TILE 16
#define LAND_LANE_THREE_TILE 13
#define LAND_LANE_ONE_HORSES 4
#define LAND_LANE_TWO_HORSES 3
#define LAND_LANE_THREE_WAGONS 3
#define WATER_LANES 4
#define WATER_LANE_HEIGHT 2
#define WATER_LANE_ONE_TILE 9
#define WATER_LANE_TWO_TILE 7
#define WATER_LANE_THREE_TILE 5
#define WATER_LANE_FOUR_TILE 3
#define WATER_LANE_ONE_LOGS 4
#define WATER_LANE_TWO_LOGS 3
#define WATER_LANE_THREE_LOGS 2
#define WATER_LANE_FOUR_LOGS 1
#define GAME_FONT_SIZE 26
#define SMALL_FONT 14
#define AKNOWLEDGEMENT_Y_TILE 23
#define AKNOWLEDGEMENT_ONE_X 5
#define AKNOWLEDGEMENT_TWO_X 700
#define WIN_MESSAGE_X 150

//Constants for the rabbit
#define RABBIT_ACTIONS 3
#define RABBIT_IDLE_FRAMES 1
#define RABBIT_WALKING_FRAMES 8
#define RABBIT_DYING_FRAMES 9
#define RABBIT_HEIGHT 96
#define RABBIT_WIDTH 96
#define RABBIT_SPEED 4
#define RABBIT_DEADZONE_X 30
#define RABBIT_DEADZONE_Y 30
#define RABBIT_START_LOCATION_Y 22

//Constants for the horses
#define HORSE_ACTIONS 2
#define HORSE_IDLE_FRAMES 1
#define HORSE_MOVING_FRAMES 12
#define HORSE_HEIGHT 144
#define HORSE_WIDTH 144
#define LIGHT_HORSE_SPEED 3
#define DARK_HORSE_SPEED 8
#define HORSE_DEADZONE_X 20
#define HORSE_DEADZONE_Y 40

//Constants for the wagon
#define WAGON_ACTIONS 2
#define WAGON_IDLE_FRAMES 1
#define WAGON_MOVING_FRAMES 4
#define WAGON_HEIGHT 160
#define WAGON_WIDTH 160
#define WAGON_SPEED 4
#define WAGON_DEADZONE_X 30
#define WAGON_DEADZONE_Y 30

//Constants for the Logs
#define LOG_ACTIONS 2
#define LOG_FRAMES 1
#define LOG_WIDTH 160
#define LOG_HEIGHT 160
#define SLOW_LOG_SPEED 2
#define MEDIUM_LOG_SPEED 4
#define FAST_LOG_SPEED 6
#define LOG_DEADZONE_X 25
#define LOG_DEADZONE_Y 55

using namespace System;
using namespace System::Drawing;

/*
	Author: Kierran McPherson
	Purpose: This class defines the game and performs all the logic to 
			 set up and play the game of frogger (rabbit edition)
*/

ref class GameManager
{

private:
	Graphics^ formGraphics;
	Graphics^ buffer;
	Bitmap^ offScreen;
	LinkedList^ landNPCS;
	LinkedList^ waterNPCS;
	LinkedList^ rabbits;
	Sprite^ rabbit;
	array<String^,2>^ rabbitFilenames;
	array<int>^ rabbitFrames;
	array<int>^ rabbitFrameHeight;
	array<int>^ rabbitFrameWidth;
	TileMap^ map;
	ViewPort^ viewPort;
	Random^ rand;
	Size clientArea;
	Rectangle world;
	array<Rectangle>^ landLanes;
	array<Rectangle>^ waterLanes;
	int lives;
	bool gameover;

	//The start button
	Button^ playAgainButton;

private:
	void CreateLaneSprites(int tileX, int tileY, int numSprites, Rectangle lane, LinkedList^ listToAdd, array<String^,2>^ filenames, array<int>^ numberOfFrames, array<int>^ frameWidth, array<int>^ frameHeight, int speed, int deadzoneX, int deadzoneY, bool center, EEdgeHitAction edgeAction, ESpriteCollisionAction collisionAction, EDirection direction);
	void CreateRabbit();
public:
	GameManager(Graphics^ startFormGraphics, Size startClientArea, Random^ startRand, Button^ startButton);

	void Reset();
	void Tick();
	void OnKeyDown(Keys e);
	void OnKeyUp(Keys e);

	bool IsGameOver()	{return gameover;}
};

