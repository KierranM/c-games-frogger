#include "stdafx.h"
#include "SpriteSheet.h"


SpriteSheet::SpriteSheet(String^ filename, int startNFrames, int frameWidth, int frameHeight)
{
	sheet = gcnew Bitmap(filename);
	currentFrame = 0;
	nFrames = startNFrames;
	frameSize = Size(frameWidth, frameHeight);
	Color transparentColor = sheet->GetPixel(0,0);
	

	//Set up the collision map
	collisionMap = gcnew array<bool, 2>(frameHeight, frameWidth);

	//Loop through the rows of the frame
	for (int row = 0; row < frameHeight; row++)
	{
		//Loop through each column in a single frame of the sheet
		for (int column = 0; column < frameWidth; column++)
		{
			Color pixelColorAtIndex = sheet->GetPixel(column, row);

			if (pixelColorAtIndex != transparentColor)
			{
				collisionMap[row, column] = true;
			}
		}
	}
	
	sheet->MakeTransparent(transparentColor);

	//Set up the frame drawing
	frame = gcnew Bitmap(frameSize.Width, frameSize.Height);
	frameGraphics = Graphics::FromImage(frame);


}

Bitmap^ SpriteSheet::GetFrame()
{
	//Clear the frameGraphics
	frameGraphics->Clear(Color::Transparent);

	//Calculate the source and destination rectangles
	Rectangle destRect = Rectangle(0,0,frame->Width, frame->Height);
	Rectangle srcRect = Rectangle(currentFrame * frameSize.Width, 0, frameSize.Width, frameSize.Height);

	//Draw the frame to the frame bitmap
	frameGraphics->DrawImage(sheet,destRect, srcRect, GraphicsUnit::Pixel);

	//If the program is in uber debugging mode show the areas of the frame that are collidable
	#if (_DEBUG && SHOW_COLLISION_MAPS)
		for (int row = 0; row < collisionMap->GetLength(0); row++)
		{
			for (int column = 0; column < collisionMap->GetLength(1); column++)
			{
				if (collisionMap[row, column] == true)
				{
					frame->SetPixel(column, row, Color::Black);
				}
			}
		}
	#endif
	//Return the frame bitmap
	return frame;
}

void SpriteSheet::UpdateCurrentFrame()
{
	currentFrame += 1;
	currentFrame = currentFrame % nFrames;
}

void SpriteSheet::ResetCurrentFrame()
{
	currentFrame = 0;
}
