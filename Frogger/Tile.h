#pragma once
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

#define GRASS 0
#define ROAD 1
#define WATER 2
#define GRASS_TO_ROAD 3
#define ROAD_TO_GRASS 4
#define GRASS_TO_WATER 5
#define WATER_TO_CLIFF 6
#define CLIFF_TO_GRASS 7
#define CLIFF_INWARDS 8
#define WATER_TO_CLIFF_INWARDS 9
#define CLIFF_OUTWARDS 10
#define WATER_TO_CLIFF_OUTWARDS 11
#define RABBIT_HOLE 12

/*
	Author: Kierran McPherson
	Purpose: This class defines a Tile which is simply a small bitmap that has some 
			 properties indicating its interactions with sprites
*/
ref class Tile
{
private:
	Bitmap^ tileImage;
	bool isWalkable;
	bool isLethal;
	bool isSolid;

public:
	Tile(String^ filename, bool startWalkable, bool startIsLethal, bool startIsSolid);
	
	Bitmap^ GetTileImage()	{return tileImage;}
	void SetTileImage(Bitmap^ image)	{tileImage = image;}

	bool GetIsWalkable()	{return isWalkable;}
	void GetIsWalkable(bool walkable)	{isWalkable = walkable;}

	bool GetIsLethal()	{return isLethal;}
	void SetIsLethal(bool lethal)	{isLethal = lethal;}

	bool GetIsSolid()	{return isSolid;}
	void SetIsSolid(bool solid)	{isSolid = solid;}
};

