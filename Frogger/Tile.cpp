#include "stdafx.h"
#include "Tile.h"


Tile::Tile(String^ filename, bool startWalkable, bool startIsLethal, bool startIsSolid)
{
	tileImage = gcnew Bitmap(filename);
	isWalkable = startWalkable;
	isLethal = startIsLethal;
	isSolid = startIsSolid;
}
